#!/bin/bash
yum remove docker docker-common docker-selinux docker-engine -y
yum install -y yum-utils device-mapper-persistent-data lvm2
#yum-config-manager \
#    --add-repo \
#    https://download.docker.com/linux/centos/docker-ce.repo
#yum makecache fast
#yum list docker-ce --showduplicates | sort -r
yum install -y docker-ce
#yum install docker-ce-18.03.0.ce-1.el7.centos
#yum install docker-ce-18.06.2.ce-3.el7 -y
systemctl start docker
#yum install -y python-setuptools
#easy_install pip
#pip install docker-compose
wget -c test.wgmf.com/mirrors/docker-compose/docker-compose -O /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
wget -c test.wgmf.com/mirrors/docker-compose/bash_completion.d/docker-compose -O /etc/bash_completion.d/docker-compose
docker run hello-world
