#!/bin/bash
echo "----------------------------------------------------------------------"
echo "close some services"
{
systemctl disable auditd 
systemctl disable chronyd
systemctl disable firewalld
systemctl disable NetworkManager
systemctl disable postfix 
systemctl disable system-udev 
systemctl disable kdump 
systemctl disable tuned 
systemctl disable wpa_supplicant 
systemctl disable dbus
systemctl disable auditd 

}  > /dev/null 2>&1

#set DNS server
{
cat <<'XUNLEI'
nameserver 114.114.114.114
nameserver 119.29.29.29
XUNLEI
} > /etc/resolv.conf 

#Disable SeLinux
echo "Disable SeLinux"
setenforce 0
if [ -s /etc/selinux/config ]; then
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
fi

#optimizer sshd_config
echo "optimizer sshd_config"
sed -i "s/#UseDNS yes/UseDNS no/" /etc/ssh/sshd_config
sed -i "s/^GSSAPIAuthentication yes/GSSAPIAuthentication no/" /etc/ssh/sshd_config
sed -i "s/^#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/" /etc/ssh/sshd_config

#close IPV6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6
{
cat <<'XUNLEI'
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
XUNLEI
} > /etc/sysctl.d/disable_ipv6.conf

#add exec(x) Permissions
chmod +x /etc/rc.d/rc.local
#set git-completion
if [ -f /usr/share/doc/git-1.8.3.1/contrib/completion/git-completion.bash ]; then
	cp /usr/share/doc/git-1.8.3.1/contrib/completion/git-completion.bash ~/.git-completion.bash
	echo 'source ~/.git-completion.bash' >> ~/.bashrc
	source ~/.git-completion.bash
fi
#set optimizer for /etc/fstab.conf
sed -i 's/defaults/defaults,noatime/' /etc/fstab
mount -a
#set sysctl kernel optimizer
cat > /etc/sysctl.d/10-sysctl.conf << EOF
 net.ipv4.ip_forward = 0
 net.ipv4.conf.default.rp_filter = 1
 net.ipv4.conf.default.accept_source_route = 0
 kernel.sysrq = 0
 kernel.core_uses_pid = 1
 net.ipv4.tcp_syncookies = 1
 kernel.msgmnb = 65536
 kernel.msgmax = 65536
 kernel.shmmax = 68719476736
 kernel.shmall = 4294967296
 net.ipv4.tcp_max_tw_buckets = 6000
 net.ipv4.tcp_sack = 1
 net.ipv4.tcp_window_scaling = 1
 net.ipv4.tcp_rmem = 4096 87380 4194304
 net.ipv4.tcp_wmem = 4096 16384 4194304
 net.core.wmem_default = 8388608
 net.core.rmem_default = 8388608
 net.core.rmem_max = 16777216
 net.core.wmem_max = 16777216
 net.core.netdev_max_backlog = 262144
 net.core.somaxconn = 262144
 net.ipv4.tcp_max_orphans = 3276800
 net.ipv4.tcp_max_syn_backlog = 262144
 net.ipv4.tcp_timestamps = 0
 net.ipv4.tcp_synack_retries = 1
 net.ipv4.tcp_syn_retries = 1
 net.ipv4.tcp_tw_recycle = 1
 net.ipv4.tcp_tw_reuse = 1
 net.ipv4.tcp_mem = 94500000 915000000 927000000
 net.ipv4.tcp_fin_timeout = 3
 net.ipv4.tcp_keepalive_time = 120
 net.ipv4.ip_local_port_range = 1024 65535
EOF
/sbin/sysctl -p
echo "sysctl set OK!!"

echo "lock some users"
{
passwd -l xfs
passwd -l news
passwd -l nscd
passwd -l dbus
passwd -l vcsa
passwd -l games
passwd -l nobody
passwd -l avahi
passwd -l haldaemon
passwd -l gopher
passwd -l ftp
passwd -l mailnull
passwd -l pcap
passwd -l mail
passwd -l shutdown
passwd -l halt
passwd -l uucp
passwd -l operator
passwd -l sync
passwd -l adm
passwd -l lp

} > /dev/null 2>&1

echo "set important files md5"
cat > /tmp/list << EOF
/bin/ping
/bin/finger
/usr/bin/who
/usr/bin/w
/usr/bin/locate
/usr/bin/whereis
/sbin/ifconfig
/bin/pico
/bin/vi
/usr/bin/vim
/usr/bin/which
/usr/bin/gcc
/usr/bin/make
/bin/rpm
/bin/ps
/bin/netstat
EOF

echo "###########################" >>/var/log/`hostname`.log
echo `date` >>/var/log/`hostname`.log
for i in `cat /tmp/list`
do
if [ -x $i ];then
md5sum $i >> /var/log/`hostname`.log
fi
done
rm -f /tmp/list


